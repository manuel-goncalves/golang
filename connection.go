package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

// getConnection
func getConnection() *sql.DB {
	dsn := "postgres://postgres:mg2020@localhost:5432/gocrud?sslmode=disable"
	db, err := sql.Open("postgres", dsn)

	if err != nil {
		log.Fatal(err)
	}

	return db
}
