package models

import (
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// Server is the structure of the server
type Server struct {
	DB     *gorm.DB
	Router *mux.Router
}
