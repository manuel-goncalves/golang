package controllers

import (
	"net/http"

	"github.com/donkijote/restgocrud/api/res"
)

// Home is the wellcome page
func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	res.JSON(w, http.StatusOK, "Welcome To This Awesome API")

}
func HomeTwo(w http.ResponseWriter, r *http.Request) {
	res.JSON(w, http.StatusOK, "Welcome To This Awesome API")

}
