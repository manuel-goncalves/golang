package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/donkijote/restgocrud/api/auth"
	"github.com/donkijote/restgocrud/api/models"
	"github.com/donkijote/restgocrud/api/res"
	"github.com/donkijote/restgocrud/api/utils/formaterror"
	"golang.org/x/crypto/bcrypt"
)

// Login end point for route
func (server *Server) Login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		res.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		res.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user.Prepare()
	err = user.Validate("login")
	if err != nil {
		res.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	token, err := server.SignIn(user.Email, user.Password)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		res.ERROR(w, http.StatusUnprocessableEntity, formattedError)
		return
	}
	response := map[string]string{"token": token}
	res.JSON(w, http.StatusOK, response)
}

// SignIn is the func that check the user in db and also execute create token callback
func (server *Server) SignIn(email, password string) (string, error) {

	var err error

	user := models.User{}

	err = server.DB.Debug().Model(models.User{}).Where("email = ?", email).Take(&user).Error
	if err != nil {
		return "", err
	}
	err = models.VerifyPassword(user.Password, password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}
	return auth.CreateToken(user.ID)
}
